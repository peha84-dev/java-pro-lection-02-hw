package ua.peha84.hw_01;

import java.io.*;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;

public class HelloServlet extends HttpServlet {

    static final String TEMPLATE =
        "<html>" + "\n" +
        "<head>" + "\n" +
        "  <title>HELLO</title>" + "\n" +
        "</head>" + "\n" +
        "<body>" + "\n" +
        "<div style=\"display: flex;height: 90vh;align-items: center;justify-items: center;justify-content: center\">" + "\n" +
        "  <div><h1 style >Hello, <span style=\"color: #E13535\">%s</span></h1></div>" + "\n" +
        "</div>" + "\n" +
        "</body>" + "\n" +
        "</html>";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");

        resp.getWriter().println(String.format(TEMPLATE, name));
    }

    public void destroy() {
    }
}